package aplicacion;
import mates.Matematicas;
 /*
         * Copyright [2022] [Pedro Sánchez Rosillo]
        Licensed under the Apache License, Version 2.0 (the "License");
        you may not use this file except in compliance with the License.
        You may obtain a copy of the License at
        http://www.apache.org/licenses/LICENSE-2.0
        Unless required by applicable law or agreed to in writing,
        2
        */
	/**
	 *@see esta clase contiene el metodo main, llama al metodo generarNumeroPiIterativo que es de la clase Matematicas y le pasa un parámetro que es el numero total de dardos lanzados.
	 *@version java 11.0.12 2021-07-20 LTS
	 *@author Pedro Sánchez
	 */



public class Principal{
	public static void main(String[] args){
		System.out.println("El número PI es " +
			Matematicas.generarNumeroIterativo(Integer.parseInt(args[0]), 0, 0));

	}
}
