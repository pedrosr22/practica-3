
package mates;
import java.util.stream.Stream;
public class Matematicas{
	/*
	 * Copyright [2022] [Pedro Sánchez Rosillo]
	 Licensed under the Apache License, Version 2.0 (the "License");
	 you may not use this file except in compliance with the License.
	 You may obtain a copy of the License at
http://www.apache.org/licenses/LICENSE-2.0
Unless required by applicable law or agreed to in writing,
2
*/
	/**
	 * @author Pedro Sánchez 
	 * @param el parametro que le paso es de tipo long, al cual he nombrado pasos que significa en el teorema de montecarlo los dardos que se lanzan.
	 * @return el metedo descrito de generarNumeroPiIterativo() lo que hace es devolver un numero de tipo double es decir con varios decimales, que se debe aproximar lo maximo al numero pi.
	 */
	public static double generarNumeroPiIterativo(long pasos){

		return Stream.generate(()->Math.pow(Math.random(),2)+ Math.pow(Math.random(),2)).limit(pasos).filter(x->x<=1).count()*4/(double)pasos;
	}	
}
